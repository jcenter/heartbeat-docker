FROM alpine:3.10

MAINTAINER wangkun23 <845885222@qq.com>

ENV VERSION=7.8.1

WORKDIR /usr/share/heartbeat
RUN apk add --update-cache curl bash libc6-compat && \
    rm -rf /var/cache/apk/* && \
    curl https://artifacts.elastic.co/downloads/beats/heartbeat/heartbeat-${VERSION}-linux-x86_64.tar.gz -o heartbeat.tar.gz && \
    tar xzvf heartbeat.tar.gz && \
    rm heartbeat.tar.gz && \
    mv heartbeat-${VERSION}-linux-x86_64/* /usr/share/heartbeat/

VOLUME /usr/share/heartbeat/data

CMD ["./heartbeat","-e"]
